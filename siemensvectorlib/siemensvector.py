#! env python3
# -*- coding: utf-8 -*-

"""
Load a Vectorfile 

"""


import re
import configparser as conf


class SiemensVector():

    def __init__(self):
        self.vectors = []

    def read(self, filename):
        """ Read a Vector file from given filename """

        config = conf.ConfigParser()
        config.read(filename)

        # First get number of Vectors from SectionName
        section = config.sections()[0]
        title, number = section.split('=')

        try:
            vector_count = int(number)
        except:
            raise ValueError(
                'could not read number of vectors from %s' % section)

        self.vectors = []
        for key in iter(config[section].keys()):
            matcher = re.match(r'vector\s*\[\s*(\d+)\s*\]', str(key), re.I)
            print("Adding vector %s" %key)
            if matcher:
                # current entry is a vector
                vector_id = int(matcher.group(1))
                vec = config[section][key]
                vec_matcher = re.match(
                    r'\(\s*(-?\d+\.?\d*)\s*,\s*(-?\d+\.?\d*)\s*,\s*(-?\d+\.?\d*)\s*\)', vec)
                if vec_matcher:
                    self.vectors.append([float(vec_matcher.group(1)),
                                        float(vec_matcher.group(2)),
                                        float(vec_matcher.group(2))])
                else:
                    print(vec)


